Source: cronie
Section: admin
Priority: optional
Maintainer: Lance Lin <lq27267@gmail.com>
Uploaders: Georges Khaznadar <georgesk@debian.org>
Build-Depends:
    debhelper-compat (= 13),
    libpam0g-dev,
    libselinux1-dev [linux-any],
    libaudit-dev [linux-any]
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://github.com/cronie-crond/cronie
Vcs-Git: https://salsa.debian.org/debian/cronie.git
Vcs-Browser: https://salsa.debian.org/debian/cronie

Package: cronie
Architecture: any
Multi-Arch: foreign
Pre-Depends:
    ${misc:Pre-Depends},
    cron-daemon-common
Depends:
    ${shlibs:Depends},
    ${misc:Depends},
    sysvinit-utils (>=3.07-1~),
    libpam-runtime,
    sensible-utils
Recommends:
    default-mta | mail-transport-agent
Suggests:
    anacron,
    logrotate,
    checksecurity
Conflicts:
    cron,
    systemd-cron,
    bcron
Replaces:
    cron,
    systemd-cron,
    bcron
Provides: cron-daemon
Description: Process Scheduling Daemon
 cronie is a daemon that runs specified programs at scheduled times and
 optionally mails generated output to the user. It is a fork of the original
 ISC cron and contains many improvements, such as:
  * inotify support (Linux only)
  * clustering support
  * full PAM support
 .
 cronie is fully compatible with ISC cron (Debian's standard job scheduler),
 and can be used as a drop-in replacement for it.
