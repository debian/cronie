#!/bin/sh
### BEGIN INIT INFO
# Provides:          cronie
# Required-Start:    $remote_fs $syslog $time
# Required-Stop:     $remote_fs $syslog $time
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: time-based job scheduler
# Description:       cronie is a daemon that runs specified programs at
#                    scheduled times and optionally mails generated output
#                    to the user.
### END INIT INFO

# Author: Christian Kastner <ckk@debian.org>

PATH=/sbin:/usr/sbin:/bin:/usr/bin
DESC="time-based job scheduler"
NAME=cronie
DAEMON_NAME=crond
DAEMON=/usr/sbin/$DAEMON_NAME
DAEMON_ARGS=""
PIDFILE=/var/run/$DAEMON_NAME.pid
SCRIPTNAME=/etc/init.d/$NAME

# Exit if the package is not installed
[ -x $DAEMON ] || exit 0

# Read configuration variable file if it is present
[ -r /etc/default/$NAME ] && . /etc/default/$NAME

# Load the VERBOSE setting and other rcS variables
. /usr/lib/init/vars.sh

# Define LSB log_* functions.
. /usr/lib/lsb/init-functions

do_start()
{
	start_daemon -p $PIDFILE $DAEMON $DAEMON_ARGS
}

do_stop()
{
    killproc -p $PIDFILE $DAEMON
}

case "$1" in
  start)
    [ "$VERBOSE" != no ] && log_daemon_msg "Starting $DESC " "$NAME"
    do_start
    case "$?" in
		0|1) [ "$VERBOSE" != no ] && log_end_msg 0 || exit 0 ;;
		2|3) [ "$VERBOSE" != no ] && log_end_msg 1 || exit 1 ;;
	esac
    ;;
  stop)
	[ "$VERBOSE" != no ] && log_daemon_msg "Stopping $DESC" "$NAME"
	do_stop
	case "$?" in
		0|3) [ "$VERBOSE" != no ] && log_end_msg 0 || exit 0 ;;
		*) [ "$VERBOSE" != no ] && log_end_msg 1 || exit 1 ;;
	esac
    ;;
  status)
       status_of_proc -p $PIDFILE "$DAEMON" "$DAEMON_NAME" && exit 0 || exit $?
    ;;
  restart|force-reload)
	log_daemon_msg "Restarting $DESC" "$NAME"
	do_stop
    if [ $? -eq 0 ]
    then
        do_start
        case "$?" in
            0) log_end_msg 0 ;;
            1) log_end_msg 1 ;; # Old process is still running
            *) log_end_msg 1 ;; # Failed to start
        esac
    else
        # Failed to stop
        log_end_msg 1
    fi
    ;;
  *)
	echo "Usage: $SCRIPTNAME {start|stop|status|restart|force-reload}" >&2
	exit 3
	;;
esac
