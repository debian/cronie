cronie (1.7.2-4) unstable; urgency=medium

  * d/patches: Fix FTBFS with GCC-15

 -- Lance Lin <lq27267@gmail.com>  Tue, 25 Feb 2025 20:43:16 +0700

cronie (1.7.2-3) unstable; urgency=medium

  * Move to unstable

 -- Lance Lin <lq27267@gmail.com>  Mon, 08 Jul 2024 22:21:08 +0700

cronie (1.7.2-2) experimental; urgency=medium

  * Move aliased files from / to /usr (Closes: #1073727)

 -- Lance Lin <lq27267@gmail.com>  Mon, 01 Jul 2024 20:41:18 +0700

cronie (1.7.2-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Refresh patches

 -- Lance Lin <lq27267@gmail.com>  Wed, 24 Apr 2024 19:57:47 +0700

cronie (1.7.1-1) unstable; urgency=medium

  * New upstream release.
  * Refresh cronie-service-debianization.patch

 -- Lance Lin <lq27267@gmail.com>  Mon, 15 Jan 2024 21:59:46 +0700

cronie (1.7.0-2) unstable; urgency=medium

  * d/patches: Update Manpage-and-typo-fixes.patch

 -- Lance Lin <lq27267@gmail.com>  Tue, 21 Nov 2023 20:00:34 +0700

cronie (1.7.0-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.

 -- Lance Lin <lq27267@gmail.com>  Thu, 19 Oct 2023 19:19:58 +0700

cronie (1.6.1-9) unstable; urgency=medium

  * d/control: Drop dependency on adduser (Closes: #1051839)
  * d/cronie.postinst: Trim duplicated code that is in cron-daemon-common
  * d/etc: Remove files installed with cron-daemon-common

 -- Lance Lin <lq27267@gmail.com>  Wed, 13 Sep 2023 20:33:14 +0700

cronie (1.6.1-8) unstable; urgency=medium

  * reorganized debian/control like the same file in cron package.
    Closes: #1046898
 -- Georges Khaznadar <georgesk@debian.org>  Wed, 16 Aug 2023 09:11:52 +0200

cronie (1.6.1-7) unstable; urgency=medium

  * Move to unstable (Closes: #1043361)
  * d/watch: Update to grab correct source file
  * d/copyright: Update year to 2023 for debian files
  * d/control: Update standards version to 4.6.2 and update maintainer email
  * d/upstream/metadata: Add upstream metadata

 -- Lance Lin <lq27267@gmail.com>  Mon, 07 Aug 2023 22:49:27 +0700

cronie (1.6.1-6) experimental; urgency=medium

  [lq27267@gmail.com]
  * Fix watch file to grab most recent upstream release

  [georgesk@debian.org]
  * replaced the dependency on lsb-base by a dependency on sysvinit-utils,
    and removed the versioned mention about the dependency on libpam-runtime

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 06 Aug 2023 21:43:34 +0200

cronie (1.6.1-5) experimental; urgency=medium

  * cronie pre-depends now on cron-daemon-common, and does no longer install
    conflicting files. Closes: #1023325

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 13 Nov 2022 17:09:31 +0100

cronie (1.6.1-4) experimental; urgency=medium

  * fixed the watch file

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 01 Nov 2022 21:53:41 +0100

cronie (1.6.1-3) experimental; urgency=medium

  * reverted previous changes, as I did not notice the closed ITA bug
    #974038, came back to contents authored by Lin Lance.

 -- Georges Khaznadar <georgesk@debian.org>  Thu, 12 May 2022 10:35:36 +0200

cronie (1.6.1-2) experimental; urgency=medium

  * refreshed debian patches, which were targetted to version 1.5.5

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 10 May 2022 17:56:24 +0200

cronie (1.6.1-1) experimental; urgency=medium

  * New upstream version (1.6.1)
  * Refreshed patches
  * d/control: New maintainer (Closes: #974038)
  * d/control: Updated standards (4.6.0), dh-compat (13)

 -- Lance Lin <LQi254@protonmail.com>  Tue, 03 May 2022 21:21:59 +0700

cronie (1.5.5-3) experimental; urgency=medium

  * Add Hurd-workaround-for-PATH_MAX.patch (Closes: #638048)
  * Fix spelling error in previous changelog entry

 -- Christian Kastner <ckk@debian.org>  Tue, 05 Nov 2019 08:04:37 +0100

cronie (1.5.5-2) experimental; urgency=medium

  * Don't build /usr/sbin/anacron
    /usr/sbin/anacron is still provided by src:anacron.
    Thanks, Andreas Beckmann, for catching this! (Closes: #944024)

 -- Christian Kastner <ckk@debian.org>  Sun, 03 Nov 2019 11:12:33 +0100

cronie (1.5.5-1) experimental; urgency=medium

  * New upstream version 1.5.5
  * Drop patches (included upstream):
    - crond-report-missing-newline-before-EOF.patch
    - crontab-Add-Y-N-to-retry-prompt.patch
    - crontab-fsync-to-check-for-full-disk.patch
    - crontab.1-Various-fixes-and-improvements.patch
    - entries-Explicitly-validate-upper-ranges-and-steps.patch
  * Refresh patches
    - debian/patches/Manpage-and-typo-fixes.patch
    - debian/patches/Rename-PAM-service-to-cronie.patch
      + Split out Unbundle-upstream-PAM-config.patch from this one

 -- Christian Kastner <ckk@debian.org>  Thu, 31 Oct 2019 22:20:05 +0100

cronie (1.5.4-final-2) experimental; urgency=medium

  * build: Set default EDITOR to /usr/bin/sensible-editor
  * d/patches (added):
    - crond-report-missing-newline-before-EOF.patch
    - entries-Explicitly-validate-upper-ranges-and-steps.patch
    - crontab.1-Various-fixes-and-improvements.patch
    - crontab-Add-Y-N-to-retry-prompt.patch
    - crontab-fsync-to-check-for-full-disk.patch

 -- Christian Kastner <ckk@debian.org>  Wed, 30 Oct 2019 21:12:01 +0100

cronie (1.5.4-final-1) experimental; urgency=medium

  * New upstream release. (Closes: #697811, #783856)

  [ Andreas Henriksson ]
  * debian/watch: update for cronie move to github
  * Modify patches to apply against new upstream release
  * Add debian/gbp.conf
  * Adjust and ship the cronie.service file
  * Use debian/clean to remove src/cron-paths.h
  * Fix lintian warning about not using default-mta
  * Fix typo in patch tagging meta-header

  [ Christian Kastner ]
  * d/control:
    - Switch Build-Depends from debhelper to debhelper-compat
    - Bump debhelper compatibility level to 12
    - Bump Standards-Version to 4.4.1 (no changes needed)
    - Remove now obsolete d/compat file
    - Set Rules-Requires-Root: no
      We don't need (fake)root for building the package.
    - Point Homepage to GitHub
    - Set Vcs-* URLs for Salsa
    - Mark package cronie as Multi-Arch: foreign
    - Add Pre-Depends: ${misc:Pre-Depends} to binary package
      As recommended by lintian's skip-systemd-native-flag-missing-pre-depends
  * d/cronie.default: Add new daemon flag "-P"
  * d/rules:
    - Add hardening flags
    - Stop passing actions to dh_installinit
      This has been obsoleted by dependency-based booting in Wheezy, see
      https://lists.debian.org/debian-devel/2013/05/msg01109.html
    - Use DEB_HOST_ARCH_OS from /usr/share/dpkg/architecture.mk
    - Proper passing of CFLAGS through DEB_CFLAGS_MAINT_APPEND
  * d/copyright:
    - Fix syntax errors
    - Switch URL to official policy URL
    - Point Source to GitHub
      fedorahosted.org has been retired
    - Bump copyrights
  * d/clean: Remove generated files for (non-enabled) anacron build
  * Sync maintscripts with src:cron

 -- Christian Kastner <ckk@debian.org>  Mon, 28 Oct 2019 19:35:38 +0100

cronie (1.4.8-1~exp1) experimental; urgency=low

  * Initial release (Closes: #590876)
  * debian/patches added:
    - 0001-Unbundle-anacron
    - 0002-Manpage-and-typo-fixes
    - 0003-Rename-PAM-service-to-cronie
    - 0004-Debian-specific-paths-and-features
    - 0005-Extend-support-for-kFreeBSD-and-GNU-HURD

 -- Christian Kastner <debian@kvr.at>  Tue, 26 Jul 2011 14:00:34 +0200
